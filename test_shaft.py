"""
test_shaft.py

Author: Andrew May

Date Modified: 12/10/2022

=====================================

This program performs 5 unit tests on the main SHAFT.py

program, and reports their success/failure.

"""

import unittest
from SHAFT import *


class MyTestCase(unittest.TestCase):

    def setUp(self):
        """This function sets up the test case."""
        self.d = '1947'
        self.h = '0xFF CC'
        self.b = '11010011'
        self.fs = '0x424D'
        self.end = 'AFDE'

    def tearDown(self):
        """This function destroys the test case (garbage collection)."""
        del self.d
        del self.h
        del self.b
        del self.fs
        del self.end

    def test_hex_value(self):
        """This function tests the validity of user input (hexadecimal)"""
        if HexTranslator.hex_set.issuperset(self.h):
            pass
        else:
            raise ValueError("Variable 'q' is not a hexadecimal value!")

    def test_bin_value(self):
        """This function tests the validity of user input (binary)"""
        if BinTranslator.bin_set.issuperset(self.b):
            pass
        else:
            raise ValueError("Variable 'b' is not a binary value!")

    def test_dec_value(self):
        """This function tests the validity of user input (decimal)"""
        if HexTranslator.dec_set.issuperset(self.d):
            pass
        else:
            raise ValueError("Variable 'd' is not a decimal value!")

    def test_file_signature(self):
        """This function tests for valid hex input,
        as well as its presence in the 'file signature' dictionary"""
        if HexTranslator.hex_set.issuperset(self.fs):
            if self.fs in FileSignatures.signatures_dict:
                pass
            else:
                raise ValueError("Variable 'fs' is not in the list of file signatures!")
        else:
            raise ValueError("Variable 'fs' is not a hexadecimal value!")

    def test_if_endian_compatible(self):
        """This function tests if a given value is hexadecimal,
        and that all the bytes entered are complete (evenly divisible by 2)"""
        if HexTranslator.hex_set.issuperset(self.end):
            if len(self.end) % 2 == 0:
                pass
            else:
                raise ValueError("Variable 'end' is not divisible by two (missing hex nibble)!")
        else:
            raise ValueError("Variable 'end' is not in hexadecimal!")


if __name__ == '__main__':
    unittest.main()
