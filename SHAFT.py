"""
S.H.A.F.T. (v.1.0.4): 'The Swiss Army Knife of Forensic Tools'

(a.k.a.: The Super Handy Assembly of Forensic Tools)

Author: Andrew May

Date Modified: 12/16/2022

========================================================

This program combines the following capabilities into one, convenient graphical (GUI) interface:

Hexadecimal decoder/encoder
Binary decoder/encoder
MD5/SHA1/SHA256 hash generator for strings and files (File I/O)
File Signature Identifier (Dictionary)
NTFS/FAT Offset cheat sheets (Database Connectivity)


*UPDATE: Added signed integer (+/-) conversion capabilities to Hexadecimal
& Binary decoders

"""

import tkinter
from ast import literal_eval
from bitstring import BitArray
from tkinter import *
from tkinter import filedialog
from tkinter import scrolledtext
import hashlib
import datetime
import sqlite3
from sqlite3 import Error

# If you need to change GUI widget colors for any reason, please do so right here!
# All visible components are controlled by (at least) one of these six color variables.
dark_color = "#3F3F42"
dark_font = "white"
dark_box = "black"

light_color = "light gray"
light_font = "black"
light_box = "white"


def current_date():
    """
    This function determines and returns the current date/time (UTC)
    :return: Date/Time
    """
    current_time = str(datetime.datetime.utcnow())
    time_string = "Session Start Date/Time (UTC): " + current_time
    return time_string


class LightDark:
    """This class allows users to choose between the light and dark versions of the interface"""
    @staticmethod
    def light_mode():
        """
        This method provides the functionality for the GUI's 'light mode'
        :return:
        """
        t.config(background=light_color)
        day_night.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        day_night.config(text="Dark Mode", command=LightDark.dark_mode)
        hex_dec_label.config(background=light_color, foreground=light_font)
        arrow1.config(background=light_color, foreground=light_font)
        entry1.config(background=light_box, foreground=light_font, insertbackground=light_font)
        hex_out.config(background=light_box, foreground=light_font, insertbackground=light_font)
        hex_button.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        dec_button.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        endian_button.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        entry2.config(background=light_box, foreground=light_font, insertbackground=light_font)
        bin_dec_label.config(background=light_color, foreground=light_font)
        bin_out.config(background=light_box, foreground=light_font, insertbackground=light_font)
        bin_button.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        dec_button1.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        arrow2.config(background=light_color, foreground=light_font)
        hash_out.config(background=light_box, foreground=light_font, insertbackground=light_font)
        hash_label.config(background=light_color, foreground=light_font)
        md5_button.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        sha1_button.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        sha256_button.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        entry3.config(background=light_box, foreground=light_font, insertbackground=light_font)
        arrow3.config(background=light_color, foreground=light_font)
        hash_label1.config(background=light_color, foreground=light_font)
        button_explore.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        md5_button1.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        sha1_button1.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        sha256_button1.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        hash_out2.config(background=light_box, foreground=light_font, insertbackground=light_font)
        file_explorer.config(background=light_box, foreground=light_font, insertbackground=light_font)
        arrow4.config(background=light_color, foreground=light_font)
        sign_label.config(background=light_color, foreground=light_font)
        arrow5.config(background=light_color, foreground=light_font)
        file_sign.config(background=light_box, foreground=light_font, insertbackground=light_font)
        search_button.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        sign_out.config(background=light_box, foreground=light_font, insertbackground=light_font)
        cheat_sheet.config(background=light_box, foreground=light_font, insertbackground=light_font)
        fat_button.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        ntfs_button.config(background=light_color, foreground=light_font, activebackground=light_color, activeforeground=light_font)
        bottom_bar.config(foreground=light_font, background=light_box)

    @staticmethod
    def dark_mode():
        """
        This method provides the functionality for the GUI's 'dark mode'
        :return:
        """
        t.config(background=dark_color)
        day_night.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        day_night.config(text="Light Mode", command=LightDark.light_mode)
        hex_dec_label.config(background=dark_color, foreground=dark_font)
        arrow1.config(background=dark_color, foreground=dark_font)
        entry1.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)
        hex_out.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)
        hex_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        dec_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        endian_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        entry2.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)
        bin_dec_label.config(background=dark_color, foreground=dark_font)
        bin_out.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)
        bin_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        dec_button1.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        arrow2.config(background=dark_color, foreground=dark_font)
        hash_out.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)
        hash_label.config(background=dark_color, foreground=dark_font)
        md5_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        sha1_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        sha256_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        entry3.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)
        arrow3.config(background=dark_color, foreground=dark_font)
        hash_label1.config(background=dark_color, foreground=dark_font)
        button_explore.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        md5_button1.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        sha1_button1.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        sha256_button1.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        hash_out2.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)
        file_explorer.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)
        arrow4.config(background=dark_color, foreground=dark_font)
        sign_label.config(background=dark_color, foreground=dark_font)
        arrow5.config(background=dark_color, foreground=dark_font)
        file_sign.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)
        search_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        sign_out.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)
        cheat_sheet.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)
        fat_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        ntfs_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
        bottom_bar.config(foreground=dark_font, background=dark_box)


class HexTranslator:
    """This class contains all the code needed for hexadecimal <-> decimal translation"""
    hex_set = set("1234567890ABCDEFabcdef ")
    dec_set = set("-1234567890")

    @staticmethod
    def clear_entry1():
        """
        Deletes/clears the contents of entry1
        :return:
        """
        entry1.delete(0, 'end')

    @staticmethod
    def clear_hex():
        """
        Deletes/clears the contents of hex output textbox
        :return:
        """
        hex_out.delete('1.0', 'end')

    @staticmethod
    def remove_spaces(string):
        """
        Removes spaces from user-generated hexadecimal input
        :param string: user input (hex)
        :return:
        """
        return string.replace(" ", "")

    @staticmethod
    def swap_endian():
        """
        Converts 'Big Endian' values to 'Little Endian' values, and vice versa
        :return:
        """
        HexTranslator.clear_hex()
        string = entry1.get()
        if string == "":
            HexTranslator.output_hex("ERROR: Input cannot be empty!")
        else:
            string = HexTranslator.remove_spaces(string)
            if HexTranslator.hex_set.issuperset(string) and len(string) % 2 == 0:
                ba = bytearray.fromhex(string)
                ba.reverse()
                HexTranslator.clear_entry1()
                endian = ba.hex()
                endian = endian.upper()
                HexTranslator.output_endian(endian)
            else:
                HexTranslator.output_hex("ERROR: Invalid hex value!")

    @staticmethod
    def output_endian(string):
        """
        Replaces original hex value in entry1
        :param string: Hexadecimal string
        :return: Converted hex value
        """
        entry1.insert(END, string)

    @staticmethod
    def output_hex(string):
        """
        Prints generated output to the appropriate text box
        :param string: Calculated dec or hex value
        :return:
        """
        hex_out.insert(END, string)

    @staticmethod
    def s_hex_to_dec(hex_string):
        """
        Performs signed hex to dec conversion (includes input validation)
        :return:
        """
        b = BitArray(hex_string)
        HexTranslator.output_hex(f"Dec (Signed Int):  {b.int}")

    @staticmethod
    def u_hex_to_dec():
        """
        Performs unsigned hex to dec conversion (includes input validation)
        :return:
        """
        global entry1, hex_out
        hex_string = entry1.get()
        HexTranslator.clear_hex()
        if hex_string == "":
            HexTranslator.output_hex("Dec:  NaN")
        else:
            if str(hex_string).startswith("0x"):
                HexTranslator.output_hex("Please remove '0x' from hex string!")
            else:
                hex_string = HexTranslator.remove_spaces(hex_string)
                if HexTranslator.hex_set.issuperset(hex_string):
                    hex_string = "0x" + str(hex_string)
                    hex_string.upper()
                    dec_num = literal_eval(hex_string)
                    HexTranslator.s_hex_to_dec(hex_string)
                    HexTranslator.output_hex(f"\nDec (Unsigned Int):  {dec_num}")
                else:
                    HexTranslator.output_hex("Dec (Unsigned Int):  NaN")

    @staticmethod
    def to_neg_hex(val, nbits):
        return hex((val + (1 << nbits)) % (1 << nbits))

    @staticmethod
    def dec_to_hex():
        """
        Performs dec to hex conversion (includes input validation)
        :return:
        """
        global entry1, hex_out
        dec_num = entry1.get()
        HexTranslator.clear_hex()
        if dec_num == "":
            HexTranslator.output_hex("Hex:  NaN")
        else:
            if HexTranslator.dec_set.issuperset(dec_num):
                if int(dec_num) >= 0:
                    hex_string = hex(int(dec_num))
                    hex_string = hex_string.strip("0x")
                    hex_string = hex_string.upper()
                    HexTranslator.output_hex(f"Hex:  0x{hex_string}")
                else:
                    hex_string = HexTranslator.to_neg_hex(int(dec_num), 64)
                    hex_string = hex_string.strip("0x")
                    hex_string = hex_string.upper()
                    HexTranslator.output_hex(f"Hex (64-bit):  0x{hex_string}")

            else:
                HexTranslator.output_hex("Hex:  NaN")


class BinTranslator:
    """This class contains all the code needed for binary <-> decimal translation"""
    bin_set = set("-01b ")
    dec_set = set("-1234567890")

    @staticmethod
    def output_bin(string):
        """
        Prints generated output to the appropriate text box
        :param string: Calculated dec or bin value
        :return:
        """
        bin_out.insert(END, string)

    @staticmethod
    def clear_bin():
        """
        Deletes/clears the contents of bin textbox
        :return:
        """
        bin_out.delete('1.0', 'end')

    @staticmethod
    def dec_to_bin():
        """
        Performs dec to bin conversion (includes input validation)
        :return:
        """
        global entry2, bin_out
        BinTranslator.clear_bin()
        dec_num = entry2.get()
        if dec_num == "":
            BinTranslator.output_bin("Bin:  NaN")
        else:
            if BinTranslator.dec_set.issuperset(dec_num) is True:
                BinTranslator.output_bin(f"Bin:  {bin(int(dec_num))}")
            else:
                BinTranslator.output_bin("Bin:  NaN")

    @staticmethod
    def bin_to_dec():
        """
        Performs bin to dec conversion (includes input validation)
        :return:
        """
        global entry2, bin_out
        BinTranslator.clear_bin()
        bin_num = entry2.get()
        if bin_num == "":
            BinTranslator.output_bin("Dec:  NaN")
        else:
            if BinTranslator.bin_set.issuperset(bin_num) is True:
                bin_num = HexTranslator.remove_spaces(bin_num)
                BinTranslator.output_bin(f"Dec:  {int(bin_num, 2)}")
            else:
                BinTranslator.output_bin("Dec:  NaN")


class HashValues:
    """This class contains all the code needed for string and file hashing"""
    global file_explorer

    @staticmethod
    def clear_hash():
        """
        Deletes/clears the contents of 'string' hash textbox
        :return:
        """
        hash_out.delete('1.0', 'end')

    @staticmethod
    def clear_hash2():
        """
        Deletes/clears the contents of 'file' hash textbox
        :return:
        """
        hash_out2.delete('1.0', 'end')

    @staticmethod
    def clear_file_exp():
        """
        Deletes/clears the contents of file explorer textbox
        :return:
        """
        file_explorer.delete('1.0', 'end')

    @staticmethod
    def browse_files():
        """
        Allows the user to view and import files from Windows File Explorer
        :return:
        """
        HashValues.clear_file_exp()
        filename = filedialog.askopenfilename(initialdir="/",
                                              title="Select a File",
                                              filetypes=(("Text Files", "*.txt*"), ("All Files", "*.*")))
        file_explorer.insert(END, str(filename))

    @staticmethod
    def output_hash(string):
        """
        Prints generated output to the appropriate text box (string hash)
        :param string: Calculated hash value
        :return:
        """
        hash_out.insert(END, string)

    @staticmethod
    def output_hash2(string):
        """
        Prints generated output to the appropriate text box (file hash)
        :param string: Calculated hash value
        :return:
        """
        hash_out2.insert(END, string)

    @staticmethod
    def file_calc_md5():
        """
        Calculates MD5 hash value for designated file
        :return:
        """
        HashValues.clear_hash2()
        filename = file_explorer.get(1.0, "end-1c")
        if filename == "":
            HashValues.output_hash2("MD5: N/A")
        else:
            md5_hash = hashlib.md5()
            try:
                with open(filename, "rb") as f:
                    for byte_block in iter(lambda: f.read(4096), b""):
                        md5_hash.update(byte_block)
                    HashValues.output_hash2("MD5: " + md5_hash.hexdigest())
            except FileNotFoundError:
                HashValues.output_hash2("ERROR: File Not Found! Invalid path or filename received.")

    @staticmethod
    def string_calc_md5():
        """
        Calculates MD5 hash value for designated string
        :return:
        """
        HashValues.clear_hash()
        string = entry3.get()
        if string == "":
            HashValues.output_hash("MD5:  N/A")
        else:
            en = string.encode()
            calc_hash = hashlib.md5(en)
            HashValues.output_hash("MD5: " + str(calc_hash.hexdigest()))

    @staticmethod
    def file_calc_sha1():
        """
        Calculates SHA-1 hash value for designated file
        :return:
        """
        HashValues.clear_hash2()
        filename = file_explorer.get(1.0, "end-1c")
        if filename == "":
            HashValues.output_hash2("SHA1: N/A")
        else:
            sha1_hash = hashlib.sha1()
            try:
                with open(filename, "rb") as f:
                    for byte_block in iter(lambda: f.read(4096), b""):
                        sha1_hash.update(byte_block)
                    HashValues.output_hash2("SHA1: " + sha1_hash.hexdigest())
            except FileNotFoundError:
                HashValues.output_hash2("ERROR: File Not Found! Invalid path or filename received.")

    @staticmethod
    def string_calc_sha1():
        """
        Calculates SHA-1 hash value for designated string
        :return:
        """
        HashValues.clear_hash()
        string = entry3.get()
        if string == "":
            HashValues.output_hash("SHA1:  N/A")
        else:
            en = string.encode()
            calc_hash = hashlib.sha1(en)
            HashValues.output_hash("SHA1: " + str(calc_hash.hexdigest()))

    @staticmethod
    def file_calc_sha256():
        """
        Calculates SHA-256 hash value for designated file
        :return:
        """
        HashValues.clear_hash2()
        filename = file_explorer.get(1.0, "end-1c")
        if filename == "":
            HashValues.output_hash2("SHA256: N/A")
        else:
            sha256_hash = hashlib.sha256()
            try:
                with open(filename, "rb") as f:
                    for byte_block in iter(lambda: f.read(4096), b""):
                        sha256_hash.update(byte_block)
                    HashValues.output_hash2("SHA256: " + sha256_hash.hexdigest())
            except FileNotFoundError:
                HashValues.output_hash2("ERROR: File Not Found! Invalid path or filename received.")

    @staticmethod
    def string_calc_sha256():
        """
        Calculates SHA-256 hash value for designated string
        :return:
        """
        HashValues.clear_hash()
        string = entry3.get()
        if string == "":
            HashValues.output_hash("SHA256:  N/A")
        else:
            en = string.encode()
            calc_hash = hashlib.sha256(en)
            HashValues.output_hash("SHA256: " + str(calc_hash.hexdigest()))


class References:
    """This class contains all the code needed for FAT/NTFS database connectivity/querying
    =======================================================================
    Credit for Reference Materials: Prof. Doug Elrick, Ph.D.,
    'Forensic Examination of Windows-Supported File Systems' (Textbook), Published: 2019"""

    @staticmethod
    def clear_textbox():
        """
        Deletes/clears the contents of FAT/NTFS output window
        :return:
        """
        cheat_sheet.delete('1.0', 'end')

    @staticmethod
    def create_connection(db):
        """ Connect to a SQLite database
        :param db: filename of database
        :return connection if no error, otherwise None"""
        try:
            conn = sqlite3.connect(db)
            return conn
        except Error as err:
            print(err)
        return None

    @staticmethod
    def select_all_fat_vbr(conn):
        """Query all rows of fat_vbr table
        :param conn: the connection object
        :return:
        """
        cur = conn.cursor()
        cur.execute("SELECT * FROM fat_vbr")
        rows = cur.fetchall()
        return rows

    @staticmethod
    def select_all_fat_dir(conn):
        """Query all rows of fat_dir table
        :param conn: the connection object
        :return:
        """
        cur = conn.cursor()
        cur.execute("SELECT * FROM fat_dir")
        rows = cur.fetchall()
        return rows

    @staticmethod
    def view_fat_tables():
        """
        This method provides action for the 'Display FAT Reference Guide' button,
        displaying all rows of the 2 'FAT' tables
        :return:
        """
        References.clear_textbox()
        conn = References.create_connection("cheatsheets.db")
        cheat_sheet.insert(END, "\n*FAT32 Quick Reference Guide*\n===========================\n\nVBR Highlights (FAT32)\n==================================\n Dec | Hex | Bytes | Description \n==================================\n")
        with conn:
            rows = References.select_all_fat_vbr(conn)
            for row in rows:
                cheat_sheet.insert(END, str(row) + '\n')
        cheat_sheet.insert(END, "\n\nFAT Directory Highlights (FAT32)\n==================================\n Dec | Hex | Bytes | Description \n==================================\n")
        with conn:
            rows = References.select_all_fat_dir(conn)
            for row in rows:
                cheat_sheet.insert(END, str(row) + '\n')

    @staticmethod
    def select_all_ntfs_vbr(conn):
        """Query all rows of ntfs_vbr table
        :param conn: the connection object
        :return:
        """
        cur = conn.cursor()
        cur.execute("SELECT * FROM ntfs_vbr")
        rows = cur.fetchall()
        return rows

    @staticmethod
    def select_all_ntfs_mft(conn):
        """Query all rows of ntfs_mft table
        :param conn: the connection object
        :return:
        """
        cur = conn.cursor()
        cur.execute("SELECT * FROM ntfs_mft")
        rows = cur.fetchall()
        return rows

    @staticmethod
    def select_all_ntfs_sia(conn):
        """Query all rows of ntfs_sia table
        :param conn: the connection object
        :return:
        """
        cur = conn.cursor()
        cur.execute("SELECT * FROM ntfs_sia")
        rows = cur.fetchall()
        return rows

    @staticmethod
    def select_all_ntfs_fna(conn):
        """Query all rows of ntfs_fna table
        :param conn: the connection object
        :return:
        """
        cur = conn.cursor()
        cur.execute("SELECT * FROM ntfs_fna")
        rows = cur.fetchall()
        return rows

    @staticmethod
    def view_ntfs_tables():
        """
        This method provides action for the 'Display NTFS Reference Guide' button,
        displaying all rows of the 4 'NTFS' tables
        :return:
        """
        References.clear_textbox()
        conn = References.create_connection("cheatsheets.db")
        cheat_sheet.insert(END, "\n*NTFS Quick Reference Guide*\n============================\n\nVBR ($Boot) Highlights\n==================================\n Dec | Hex | Bytes | Description \n==================================\n")
        with conn:
            rows = References.select_all_ntfs_vbr(conn)
            for row in rows:
                cheat_sheet.insert(END, str(row) + '\n')
        cheat_sheet.insert(END, "\n\n$MFT Record Header Highlights\n==================================\n Dec | Hex | Bytes | Description \n==================================\n")
        with conn:
            rows = References.select_all_ntfs_mft(conn)
            for row in rows:
                cheat_sheet.insert(END, str(row) + '\n')
        cheat_sheet.insert(END, "\n\nSIA (0x10 00 00 00) Highlights\n==================================\n Dec | Hex | Bytes | Description \n==================================\n")
        with conn:
            rows = References.select_all_ntfs_sia(conn)
            for row in rows:
                cheat_sheet.insert(END, str(row) + '\n')
        cheat_sheet.insert(END, "\n\nFNA (0x30 00 00 00) Highlights\n==================================\n Dec | Hex | Bytes | Description \n==================================\n")
        with conn:
            rows = References.select_all_ntfs_fna(conn)
            for row in rows:
                cheat_sheet.insert(END, str(row) + '\n')


class FileSignatures:
    """This class facilitates all the hexadecimal 'file signature'
    lookup capabilities of S.H.A.F.T."""

    signatures_dict = {
        "0x47494638": "GIF (Image)",
        "0x474946383761": "GIF87a (Image)",
        "0x474946383961": "GIF89a (Image)",
        "0xFFD8FFDB": "JPG/JPEG (Image)",
        "0xFFD8FFE0": "JPG/JPEG (Image)",
        "0xFFD8FFEE": "JPG/JPEG (Image)",
        "0x504B0304": "ZIP (Archive)",
        "0x504B0506": "ZIP (Archive)",
        "0x504B0708": "ZIP (PK Zip File)",
        "0x255044462D": "PDF (Document)",
        "0x25504446": "PDF (Document)",
        "0x52494646": "WAV/AVI (Audio/Video)",
        "0xFFFB": "MP3 (Audio)",
        "0xFFF3": "MP3 (Audio)",
        "0xFFF2": "MP3 (Audio)",
        "0x494433": "MP3 (Audio)",
        "0x424D": "BMP (Bitmap Image)",
        "0x4344303031": "ISO/CDI (CD/DVD Image)",
        "0x4D546864": "MIDI (Audio)",
        "0x6B6F6C79": "DMG (Apple Disk Image)",
        "0x1F8B": "GZIP (Archive)",
        "0x3C3F786D6C20": "XML (Text/Web)",
        "0x7B5C72746631": "RTF (Document)",
        "0x000001B3": "MPG/MPEG (Video)",
        "0x72656766": "DAT/HIV (Windows Registry)",
        "0x89504E47": "PNG (Image)",
        "0xD0CF": "DOC (MSOffice Document)",
        "0xD0CF11E0A1B11AE100": "DOC (MSOffice Document)",
        "0x46726F6D": "EML (Email)",
        "0x4D5A": "EXE (Windows Executable)",
        "0x1A45DFA3": "WEBM (Video)",
        "736B6970": "MOV (Quicktime Movie)"
    }

    @staticmethod
    def output_signs(string):
        """
        Prints specified dictionary key/value to the appropriate text box
        :param string: Proper key/value pair
        :return:
        """
        sign_out.insert(END, string)

    @staticmethod
    def clear_signs():
        """
        Deletes/clears the contents of 'sign_out' textbox
        :return:
        """
        sign_out.delete('1.0', 'end')

    @staticmethod
    def input_sign():
        """
        Allows users to query a dictionary of hexadecimal file signature
        'keys' that correspond with the appropriate file extension 'values'
        :return:
        """
        global file_sign
        FileSignatures.clear_signs()
        signature = file_sign.get()
        signature = HexTranslator.remove_spaces(signature)
        if signature == "":
            FileSignatures.output_signs("Invalid file signature (cannot be NULL)!")
        else:
            if HexTranslator.hex_set.issuperset(signature):
                if str(signature).startswith("0x"):
                    signature = signature.strip("0x")
                    signature = signature.upper()
                    signature = "0x" + str(signature)
                else:
                    signature = signature.upper()
                    signature = "0x" + str(signature)
                if signature in FileSignatures.signatures_dict:
                    value = FileSignatures.signatures_dict.get(signature)
                    FileSignatures.output_signs(f"Signature: {signature}\nFile Type: " + value)
                else:
                    FileSignatures.output_signs(f"There is no record associated with file signature: {signature}!")
            else:
                FileSignatures.output_signs("Invalid file signature (must be hexadecimal)!")


"""====================================!**Start of GUI Code**!===================================="""

t = tkinter.Tk()
t.title("S.H.A.F.T. (v.1.0.4)")
t.geometry('1150x677')
t.resizable(False, False)
t.config(background=dark_color)

day_night = Button(t, text="Light Mode", command=LightDark.light_mode)
day_night.config(height=1, background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)
day_night.grid(row=0, column=0)

"""========================================================================================="""

hex_dec_label = Label(t, text='Hex/Dec Converter:')
hex_dec_label.grid(row=1, column=0, pady=3, padx=0)
hex_dec_label.config(background=dark_color, foreground=dark_font)

entry1 = Entry(t, width=25)
entry1.focus_set()
entry1.grid(row=1, column=1, pady=3, padx=0)
entry1.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)

arrow1 = Label(t, text=' ----------------> ')
arrow1.grid(row=1, column=3, pady=5)
arrow1.config(background=dark_color, foreground=dark_font)

hex_out = Text(t)
hex_out.config(width=40, height=4, relief='sunken')
hex_out.grid(row=1, column=4, pady=3)
hex_out.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)

hex_button = Button(t, text='Convert to Hex', command=HexTranslator.dec_to_hex)
hex_button.config(height=1)
hex_button.grid(row=2, column=0, pady=5)
hex_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

dec_button = Button(t, text='Convert to Dec', command=HexTranslator.u_hex_to_dec)
dec_button.config(height=1)
dec_button.grid(row=2, column=1, pady=5)
dec_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

endian_button = Button(t, text='Swap Endianness', command=HexTranslator.swap_endian)
endian_button.config(height=1)
endian_button.grid(row=3, column=0, columnspan=2, pady=5)
endian_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

"""==============================================================================================="""

bin_dec_label = Label(t, text='Bin/Dec Converter:')
bin_dec_label.grid(row=4, column=0, pady=5)
bin_dec_label.config(background=dark_color, foreground=dark_font)

entry2 = Entry(t, width=25)
entry2.focus_set()
entry2.grid(row=4, column=1, pady=5)
entry2.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)

arrow2 = Label(t, text=' ----------------> ')
arrow2.grid(row=4, column=3, pady=5)
arrow2.config(background=dark_color, foreground=dark_font)

bin_out = Text(t)
bin_out.config(width=40, height=4, relief='sunken')
bin_out.grid(row=4, column=4, pady=3)
bin_out.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)

bin_button = Button(t, text='Convert to Bin', command=BinTranslator.dec_to_bin)
bin_button.config(height=1)
bin_button.grid(row=5, column=0, pady=5)
bin_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

dec_button1 = Button(t, text='Convert to Dec', command=BinTranslator.bin_to_dec)
dec_button1.config(height=1)
dec_button1.grid(row=5, column=1, pady=5)
dec_button1.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

"""================================================================================================="""

hash_label = Label(t, text='String Hashing Tool:')
hash_label.grid(row=6, column=0, pady=5)
hash_label.config(background=dark_color, foreground=dark_font)

entry3 = Entry(t, width=25)
entry3.focus_set()
entry3.grid(row=6, column=1, pady=5)
entry3.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)

arrow3 = Label(t, text=' ----------------> ')
arrow3.grid(row=6, column=3, pady=5)
arrow3.config(background=dark_color, foreground=dark_font)

hash_out = Text(t)
hash_out.config(width=40, height=4, relief='sunken')
hash_out.grid(row=6, column=4, pady=3)
hash_out.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)

md5_button = Button(t, text='Calculate MD5', command=HashValues.string_calc_md5)
md5_button.config(height=1)
md5_button.grid(row=7, column=0, pady=5)
md5_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

sha1_button = Button(t, text='Calculate SHA1', command=HashValues.string_calc_sha1)
sha1_button.config(height=1)
sha1_button.grid(row=7, column=1, pady=5)
sha1_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

sha256_button = Button(t, text='Calculate SHA256', command=HashValues.string_calc_sha256)
sha256_button.config(height=1)
sha256_button.grid(row=8, column=0, columnspan=2, pady=5)
sha256_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

"""==========================================================================================="""

hash_label1 = Label(t, text='File Hashing Tool:')
hash_label1.grid(row=9, column=0, pady=5)
hash_label1.config(background=dark_color, foreground=dark_font)

file_explorer = Text(t)
file_explorer.config(width=20, height=4, relief='sunken')
file_explorer.grid(row=9, column=1, pady=5)
file_explorer.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)

arrow4 = Label(t, text=' ----------------> ')
arrow4.grid(row=9, column=3, pady=5)
arrow4.config(background=dark_color, foreground=dark_font)

hash_out2 = Text(t)
hash_out2.config(width=40, height=4, relief='sunken')
hash_out2.grid(row=9, column=4, pady=3)
hash_out2.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)

button_explore = Button(t, text="Browse Files",
                        command=HashValues.browse_files)
button_explore.config(height=1)
button_explore.grid(row=10, column=0, pady=5)
button_explore.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

md5_button1 = Button(t, text='Calculate MD5', command=HashValues.file_calc_md5)
md5_button1.config(height=1)
md5_button1.grid(row=10, column=1, pady=5)
md5_button1.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

sha1_button1 = Button(t, text='Calculate SHA1', command=HashValues.file_calc_sha1)
sha1_button1.config(height=1)
sha1_button1.grid(row=11, column=0, pady=5)
sha1_button1.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

sha256_button1 = Button(t, text='Calculate SHA256', command=HashValues.file_calc_sha256)
sha256_button1.config(height=1)
sha256_button1.grid(row=11, column=1, pady=5)
sha256_button1.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

"""===================================================================================="""

sign_label = Label(t, text='File Signature Lookup:')
sign_label.grid(row=12, column=0, pady=5)
sign_label.config(background=dark_color, foreground=dark_font)

file_sign = Entry(t, width=25)
file_sign.focus_set()
file_sign.grid(row=12, column=1, pady=5)
file_sign.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)

arrow5 = Label(t, text=' ----------------> ')
arrow5.grid(row=12, column=3, pady=5)
arrow5.config(background=dark_color, foreground=dark_font)

sign_out = Text(t)
sign_out.config(width=40, height=2, relief='sunken')
sign_out.grid(row=12, column=4, pady=3)
sign_out.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)

search_button = Button(t, text='Search', command=FileSignatures.input_sign)
search_button.config(height=1)
search_button.grid(row=13, column=1, pady=5)
search_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

"""=================================================================================================="""

cheat_sheet = scrolledtext.ScrolledText(t)
cheat_sheet.grid(column=9, row=1, rowspan=9)
cheat_sheet.config(width=45, height=30)
cheat_sheet.config(background=dark_box, foreground=dark_font, insertbackground=dark_font)

fat_button = Button(t, text='Display FAT Reference Guide', command=References.view_fat_tables)
fat_button.config(height=1, width=35)
fat_button.grid(row=10, column=9, pady=5)
fat_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

ntfs_button = Button(t, text='Display NTFS Reference Guide', command=References.view_ntfs_tables)
ntfs_button.config(height=1, width=35)
ntfs_button.grid(row=11, column=9, pady=5)
ntfs_button.config(background=dark_color, foreground=dark_font, activebackground=dark_color, activeforeground=dark_font)

"""=================================================================================================="""

bottom_bar = Label(t, text=f"S.H.A.F.T (v.1.0.4)\t\t\t**Designed and Developed by A.M., 2022**\t\t{current_date()}", borderwidth=1, relief="solid", foreground=dark_font, background=dark_box)
bottom_bar.config(height=1, width=170)
bottom_bar.grid(row=15, column=0, columnspan=20, sticky='sw')

t.mainloop()
