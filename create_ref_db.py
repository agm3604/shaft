"""
create_ref_db.py

Author: Andrew May

Date Modified: 12/11/2022

===================================

This program creates the 'cheatsheets.db' database file,

sets up its 6 distinct tables, and allows users to append data

to these tables.

===================================================================

*PLEASE NOTE: This file should NOT be tampered with unless one of the following conditions are met:

1. A fatal error has occurred in the 'cheatsheets.db' file

2. Data contained within the database needs to be corrected, updated, deleted, etc.

3. You just want to watch the world (and this project) burn...

"""

import sqlite3
from sqlite3 import Error


def create_connection(db):
    """ Connect to a SQLite database
    :param db: filename of database
    :return connection if no error, otherwise None"""
    try:
        conn = sqlite3.connect(db)
        return conn
    except Error as err:
        print(err)
    return None


def create_table(conn, sql_create_table):
    """ Creates table with given SQL statement
    :param conn: Connection object
    :param sql_create_table: a SQL CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(sql_create_table)
    except Error as e:
        print(e)


def create_tables(database):
    """This function generates all the tables in the database."""
    sql_create_fat_vbr_table = """CREATE TABLE IF NOT EXISTS fat_vbr (
                                        dec_offset text NOT NULL,
                                        hex_offset text NOT NULL,
                                        bytes text NOT NULL,
                                        description text NOT NULL
                                    ); """

    sql_create_fat_dir_table = """CREATE TABLE IF NOT EXISTS fat_dir (
                                        dec_offset text NOT NULL,
                                        hex_offset text NOT NULL,
                                        bytes text NOT NULL,
                                        description text NOT NULL
                                    ); """

    sql_create_ntfs_vbr_table = """CREATE TABLE IF NOT EXISTS ntfs_vbr (
                                        dec_offset text NOT NULL,
                                        hex_offset text NOT NULL,
                                        bytes text NOT NULL,
                                        description text NOT NULL
                                );"""
    sql_create_ntfs_mft_table = """CREATE TABLE IF NOT EXISTS ntfs_mft (
                                        dec_offset text NOT NULL,
                                        hex_offset text NOT NULL,
                                        bytes text NOT NULL,
                                        description text NOT NULL
                                );"""

    sql_create_ntfs_sia_table = """CREATE TABLE IF NOT EXISTS ntfs_sia (
                                        dec_offset text NOT NULL,
                                        hex_offset text NOT NULL,
                                        bytes text NOT NULL,
                                        description text NOT NULL
                                );"""

    sql_create_ntfs_fna_table = """CREATE TABLE IF NOT EXISTS ntfs_fna (
                                        dec_offset text NOT NULL,
                                        hex_offset text NOT NULL,
                                        bytes text NOT NULL,
                                        description text NOT NULL
                                );"""

    # create a database connection
    conn = create_connection(database)
    if conn is not None:
        create_table(conn, sql_create_fat_vbr_table)
        create_table(conn, sql_create_fat_dir_table)
        create_table(conn, sql_create_ntfs_vbr_table)
        create_table(conn, sql_create_ntfs_mft_table)
        create_table(conn, sql_create_ntfs_sia_table)
        create_table(conn, sql_create_ntfs_fna_table)
    else:
        print("Unable to connect to " + str(database))


def create_fat_vbr(conn, fat_vbr):
    """
    Allows users to append data entries to the 'fat_vbr' table
    :param conn: Connection object
    :param fat_vbr: Passed-in data object
    :return:
    """
    sql = ''' INSERT INTO fat_vbr(dec_offset, hex_offset, bytes, description)
              VALUES(?,?,?,?) '''
    cur = conn.cursor()  # cursor object
    cur.execute(sql, fat_vbr)
    return cur.lastrowid  # returns the row id of the cursor object, the person id


def create_fat_dir(conn, fat_dir):
    """
    Allows users to append data entries to the 'fat_dir' table
    :param conn: Connection object
    :param fat_dir: Passed-in data object
    :return:
    """
    sql = ''' INSERT INTO fat_dir(dec_offset, hex_offset, bytes, description)
              VALUES(?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, fat_dir)
    return cur.lastrowid


def create_ntfs_vbr(conn, ntfs_vbr):
    """
    Allows users to append data entries to the 'ntfs_vbr' table
    :param conn: Connection object
    :param ntfs_vbr: Passed-in data object
    :return:
    """
    sql = ''' INSERT INTO ntfs_vbr(dec_offset, hex_offset, bytes, description)
              VALUES(?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, ntfs_vbr)
    return cur.lastrowid


def create_ntfs_mft(conn, ntfs_mft):
    """
    Allows users to append data entries to the 'ntfs_mft' table
    :param conn: Connection object
    :param ntfs_mft: Passed-in data object
    :return:
    """
    sql = ''' INSERT INTO ntfs_mft(dec_offset, hex_offset, bytes, description)
              VALUES(?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, ntfs_mft)
    return cur.lastrowid


def create_ntfs_sia(conn, ntfs_sia):
    """
    Allows users to append data entries to the 'ntfs_sia' table
    :param conn: Connection object
    :param ntfs_sia: Passed-in data object
    :return:
    """
    sql = ''' INSERT INTO ntfs_sia(dec_offset, hex_offset, bytes, description)
              VALUES(?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, ntfs_sia)
    return cur.lastrowid


def create_ntfs_fna(conn, ntfs_fna):
    """
    Allows users to append data entries to the 'ntfs_vbr' table
    :param conn: Connection object
    :param ntfs_fna: Passed-in data object
    :return:
    """
    sql = ''' INSERT INTO ntfs_fna(dec_offset, hex_offset, bytes, description)
              VALUES(?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, ntfs_fna)
    return cur.lastrowid


if __name__ == '__main__':
    create_tables("cheatsheets.db")
    conn = create_connection("cheatsheets.db")
    with conn:
        """ //Uncomment and run IF database gets deleted
        
        # **FAT VBR**
        entry = ("00", "00", "3", "Jump to bootstrap")
        create_fat_vbr(conn, entry)
        entry = ("03", "03", "8", "OEM name/version")
        create_fat_vbr(conn, entry)
        entry = ("11", "0B", "2", "# of bytes per sector")
        create_fat_vbr(conn, entry)
        entry = ("13", "0D", "1", "# of sectors per cluster")
        create_fat_vbr(conn, entry)
        entry = ("14", "0E", "2", "# of reserved sectors")
        create_fat_vbr(conn, entry)
        entry = ("19", "13", "2", "# of sectors in File Sys")
        create_fat_vbr(conn, entry)
        entry = ("22", "16", "2", "# of sectors per FAT")
        create_fat_vbr(conn, entry)
        entry = ("32", "20", "4", "# of sectors in File Sys")
        create_fat_vbr(conn, entry)
        entry = ("44", "2C", "4", "1st cluster of Root Dir.")
        create_fat_vbr(conn, entry)

        # **FAT Directory**
        entry = ("00", "00", "8", "Filename (1-8 Bytes)")
        create_fat_dir(conn, entry)
        entry = ("08", "08", "3", "Extension (0-3 Bytes)")
        create_fat_dir(conn, entry)
        entry = ("13", "0D", "1", "Created Time (1/10 Sec)")
        create_fat_dir(conn, entry)
        entry = ("14", "0E", "2", "Created Time")
        create_fat_dir(conn, entry)
        entry = ("16", "10", "2", "Created Date")
        create_fat_dir(conn, entry)
        entry = ("18", "12", "2", "Last Accessed Date")
        create_fat_dir(conn, entry)
        entry = ("22", "16", "2", "Mod. Time (5/6/5)")
        create_fat_dir(conn, entry)
        entry = ("24", "18", "2", "Mod. Date (7/4/5)")
        create_fat_dir(conn, entry)
        entry = ("28", "1C", "4", "File Size (Bytes)")
        create_fat_dir(conn, entry)
        
        # **NTFS VBR**
        entry = ("03", "03", "8", "OEM ID")
        create_ntfs_vbr(conn, entry)
        entry = ("11", "0B", "2", "Bytes per sector")
        create_ntfs_vbr(conn, entry)
        entry = ("13", "0D", "1", "Sectors per cluster")
        create_ntfs_vbr(conn, entry)
        entry = ("40", "28", "8", "# of sectors in part.")
        create_ntfs_vbr(conn, entry)
        entry = ("64", "40", "1", "Clusters per $MFT Rec.")
        create_ntfs_vbr(conn, entry)

        # **NTFS $MFT**
        entry = ("00", "00", "4", "FILE Signature")
        create_ntfs_mft(conn, entry)
        entry = ("16", "10", "2", "Sequence Count")
        create_ntfs_mft(conn, entry)
        entry = ("22", "16", "2", "Flags (00, 01, 02, 03)")
        create_ntfs_mft(conn, entry)
        entry = ("44", "2C", "4", "$MFT Record Number")
        create_ntfs_mft(conn, entry)
        entry = ("48", "30", ">0", "Update Seq. Number")
        create_ntfs_mft(conn, entry)

        # **NTFS SIA**
        entry = ("24", "18", "8", "Creation Time")
        create_ntfs_sia(conn, entry)
        entry = ("32", "20", "8", "Modified Time")
        create_ntfs_sia(conn, entry)
        entry = ("40", "28", "8", "$MFT Modified Time")
        create_ntfs_sia(conn, entry)
        entry = ("48", "30", "8", "Last Access Time")
        create_ntfs_sia(conn, entry)
        entry = ("56", "38", "4", "DOS Flags")
        create_ntfs_sia(conn, entry)

        # **NTFS FNA**
        entry = ("24", "18", "6", "Parent $MFT Record #")
        create_ntfs_fna(conn, entry)
        entry = ("30", "1E", "2", "Parent Seq. Number")
        create_ntfs_fna(conn, entry)
        entry = ("32", "20", "8", "Creation Time")
        create_ntfs_fna(conn, entry)
        entry = ("40", "28", "8", "Modified Time")
        create_ntfs_fna(conn, entry)
        entry = ("48", "30", "8", "$MFT Modified Time")
        create_ntfs_fna(conn, entry)
        entry = ("56", "38", "8", "Last Access Time")
        create_ntfs_fna(conn, entry)
        entry = ("88", "58", "1", "Filename Length")
        create_ntfs_fna(conn, entry)
        entry = ("90", "5A", "?", "Filename")
        create_ntfs_fna(conn, entry)
        
        //Uncomment and run IF database gets deleted """
